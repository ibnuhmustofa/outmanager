<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileMustExist extends Model
{
    protected $table = 'file_must_exists';
}
