<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Directory extends Model
{
	protected $fillable = ['parent_slug', 'name', 'slug'];
}