<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenerFile extends Model
{
   	protected $table = 'opener_files';
}
