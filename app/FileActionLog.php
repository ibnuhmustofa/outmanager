<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileActionLog extends Model
{
    protected $table = 'file_action_logs';
}
