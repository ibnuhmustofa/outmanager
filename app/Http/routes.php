<?php

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::group([
		// 'middleware'=>'auth'
	], function() {
		Route::get('/', function() {
			return view('app.home');
		});

		Route::get('browser', 'AppController@getOS');

		// Route::get('/rtrw-provinsi', 'AppController@rtRwProv');
		// Route::get('/rtrw-kabupaten', 'AppController@rtRwKab');
		// Route::get('/rtrw-kota', 'AppController@rtRwKota');
		// Route::get('/kebijakan-sektoral', 'AppController@kebSektoral');
		// Route::get('/regulasi-tata-ruang', 'AppController@regTataRuang');

		Route::get('tabulasi', 'AppController@indexTabulate');

		Route::get('search', 'AppController@search');
		
		Route::post('f/folder', 'AppController@storeFolder');
		Route::post('f/file', 'AppController@storeFile');

		Route::post('f/fon', 'AppController@updateFoldername');
		Route::post('f/fin', 'AppController@updateFilename');

		Route::post('f/fo/d', 'AppController@deleteFolder');
		Route::post('f/fi/d', 'AppController@deleteFile');

		Route::post('f/fi/mef', 'AppController@storeMustexistfile');

		Route::get('fo/fm/{location}', 'AppController@launchFoinfm'); // Launch folder in filemanager
		Route::get('ap/fm/{location}', 'AppController@launchInfm'); // Launch file in filemanager
		Route::get('ap/fi/{location}', 'AppController@launchInapp'); // Launch file in App
		
		// config
		Route::get('file', 'MenuController@indexFile');
		Route::get('config', 'MenuController@indexConfig');
		Route::get('app', 'MenuController@indexApp');
		Route::get('log', 'MenuController@indexLog');

		// Route::get('config', 'ConfigController@index');
		Route::post('config/p', 'ConfigController@updateUploadpath');

		// Route::get('config/app', 'ConfigController@app');

		// log
		Route::get('log', 'LogController@index');
		Route::get('log/updatefile', 'LogController@indexUpdatefile');

		Route::get('/{directory}', 'AppController@directory');

	});