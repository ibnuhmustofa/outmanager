<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class LogController extends Controller
{
    public function index() {
    	return view('app.log.index');
    }

    public function indexUpdatefile() {
    	return view('app.log.indexUpdatefile');
    }
}