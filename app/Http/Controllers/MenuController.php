<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Config;
use App\Directory;
use App\File;
use App\OpenerFile;


class MenuController extends Controller
{
    public function indexFile() {
    	return view('app.config.indexFile');
    }

    public function indexConfig() {
    	$config = Config::all();
    	return view('app.config.indexConfig', compact('config', $config));
	
    }

    public function indexApp() {
    	$opener = DB::table('opener_files')
				->select('*')
				->groupBy('ext')
				->get();

    	return view('app.config.indexApp', compact('opener', $opener));
    		
    }

    public function indexLog() {
    	return view('app.config.indexLog');
    		
    }
}
