<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Config;
use App\OpenerFile;
use DB;

class ConfigController extends Controller
{
    public function index() {
    	$config = Config::all();
    	return view('app.config.index', compact('config', $config));
    }

    public function updateUploadpath(Request $request) {
    	$conf = Config::findOrFail(1);

    	$conf->upload_path = $request->path;
    	$conf->save();

    	return redirect('/config');
    }

    public function app() {
    	$opener = DB::table('opener_files')
    					->select('*')
    					->groupBy('ext')
    					->get();

    	return view('app.config.app', compact('opener', $opener));
    }
}
