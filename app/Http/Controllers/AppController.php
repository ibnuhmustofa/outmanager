<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Redirect;
use App\Config;
use App\Directory;
use App\File;
use App\OpenerFile;
use App\FileMustExist;
use DB;

class AppController extends Controller
{
    public function getOS() {
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        return $platform;
    }

    public function directory($directory) {
        $dirs = Directory::where('parent_slug', '=', $directory)
                            ->orderBy('name', 'asc')
                            ->get();

        $files = File::where('parent_slug', '=', $directory)
                        ->orderBy('origin_name')
                        ->get();

        // $crumbs = DB::table('directories')
        //                 ->select('*')
        //                 ->where('top_parent', '=', $directory)
        //                 ->orderBy('parent_no', 'asc')
        //                 ->get();
                        
        // $data = array();
        // $i = 0;

        // foreach ($crumbs as $crumb) {
        //     $data[$i]['name'] = $crumb->name;
        //     $i++;
        // }


        // return $crumbs;

        return view('app.directory', [
                'dirs' => $dirs,
                'files' => $files
            ]);
    }


    public function storeFolder(Request $request) {
    	$dir = new Directory();

        $crumb = DB::table('directories')
                        ->select('*')
                        ->where('parent_slug', '=', $request->current)
                        ->groupBy('top_parent')
                        ->orderBy('parent_no', 'asc')
                        ->take(1)
                        ->get();
        // return $crumb;

        // $parent_no = $crumb++;

    	$dir->parent_slug = $request->current;
    	$dir->name = $request->folderName;
    	$dir->slug = substr(base64_encode(substr($request->current, 0, 5)), 0, -2).substr(base64_encode($dir->name), 0, 5).str_random(20);
        // $dir->top_parent = $crumb['top_parent'];
        // $dir->parent_no = $crumb['parent_no']++;
        $dir->top_parent = 0;
        $dir->parent_no = 1;
    	$dir->save();

    	return Redirect($request->current);
    }

    public function storeFile(Request $request) {
    	$data = new File();
        $opener = new OpenerFile();

 

    	$destinationPath = 'upload';
    	$file = $request->file('file');

       $mustExist = FileMustExist::where('file_name', $file->getClientOriginalName())
                            ->update(['uploaded'=>'1']);

		if($file->isValid()) {
			$data->parent_slug = $request->current;
			$data->origin_name = $file->getClientOriginalName();
            $data->alias_name = substr(base64_encode($data->origin_name), 1, 5).str_random(20);
            $data->ext = $file->getClientOriginalExtension();
            $data->location = "C:\Public\uploader\\".substr(base64_encode($request->current), 3, 5).$request->current."/".$data->alias_name;
// move upload file			
            $request->file->move($data->location, $data->origin_name);

            $opener->ext = $data->ext;

			$data->save();
            $opener->save();
		}

		return Redirect($request->current);
    }


    public function search(Request $request) {
    	$query = $request->get('query');

    	$src = File::where('origin_name', 'LIKE', '%'.$query.'%')
    						->get();

    	return view('app.search', [
                'src' => $src,
                'query' => $query
            ]);
    }

    public function launchFoinapp($location) {
        // code here
    }

    public function launchInapp($location) {
        $files = DB::table('files')
                    ->select('*')
                    ->where('alias_name', '=', $location)
                    ->get();

        $configs = Config::all();

        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        foreach ($configs as $config) {
            $config = $config;
        }

        foreach($files as $file) {
             $location = $file->location;
        }

        // $command = "pcmanfm /home/helpdesk/Public/uploader/cnRydcajRCUlo5UG90Sko2dGJRanFpRU/php.ini.configuration.largefileupload";
        // shell_exec($command)

        // if($config->$platform != $platform) {
        //     if($platform == 'linux') {
        //         $command = "pcmanfm ";
        //         shell_exec($command);
        //     } elseif ($platform == 'windows') {
        //         $command = "explorer.exe ";
        //         exec($command);
        //     }
        // } else {
        //     if($platform == "linux") {
        //         $command = "pcmanfm ";
        //     }
        // }

        if($platform == 'linux') {
            $command = "pcmanfm";
            shell_exec($command);
            // system($command);
            // echo $command;
        } else {
            $command = "explorer.exe ";
            exec($command);
            // system($command);
            // echo $command;
        }

        return Redirect::back();
    }

    public function updateFilename(Request $request) {
        $file = File::findOrFail($request->fid);

        $file->origin_name = $request->fileName;

        $file->save();

        return Redirect::back();
    }

    public function deleteFile(Request $request) {
        $file = File::findOrFail($request->fid);

        $file->delete();

        return Redirect::back();
    }

    public function updateFolderName(Request $request) {
        $folder = Directory::findOrFail($request->fod);

        $folder->name = $request->folderName;

        $folder->save();
        return Redirect::back();
    }

    public function deleteFolder(Request $request) {
        $folder = Directory::findOrFail($request->fod);

        $folder->delete();

        return Redirect::back();
    }

    public function indexTabulate() {
        // $tabus = FileMustExist::orderBy('top_folder', 'asc')
        //                         ->orderBy('file_name', 'asc')
        //                         ->get();

        $tabus = DB::table('file_must_exists')
                    ->join('directories', 'file_must_exists.parent_folder', '=', 'directories.slug')
                    ->select('file_must_exists.*', 'directories.parent_slug as main_directory', 'directories.name as sub_directory')
                    ->get();

        $dirs = Directory::where('parent_slug', '=', 'rtrw-provinsi')
                            ->get();
                            
        return view('app.tabulation.index', [
                'tabus' => $tabus,
                'dirs' => $dirs
            ]);
    }


    public function storeMustexistfile(Request $request) {
        $tabula = new FileMustExist();

        $tabula->parent_folder = $request->dir;
        $tabula->file_name = $request->fileName.".".$request->fileExt;
        $tabula->ext = $request->fileExt;
        $tabula->save();

        return Redirect::back();
    }
}
