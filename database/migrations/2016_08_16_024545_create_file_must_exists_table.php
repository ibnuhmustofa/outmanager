<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileMustExistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_must_exists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent_folder');
            $table->string('file_name');
            $table->string('ext');
            $table->integer('uploaded')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_must_exists');
    }
}
