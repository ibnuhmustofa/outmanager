<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->delete();

        DB::table('files')->insert([
        		['parent_slug'=>'rt-rw-provinsi', 'name'=>'Penduduk Jawa Tengah Menurut Kabupaten Kota dan Kelompok Umur Tahun 2013.xls', 'ext'=>'.xlsx', 'slug'=>str_random(10), 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
        		['parent_slug'=>'rt-rw-provinsi', 'name'=>'Penduduk Jawa Tengah Menurut Kabupaten Kota dan Kelompok Umur Tahun 2014.xls', 'ext'=>'.xlsx'ender/Penduduk Jawa Tengah Menurut Kabupaten Kota dan Kelompok Umur Tahun 2014.xls', 'slug'=>str_random(10), 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],


        		['parent_slug'=>base64_encode('citra-satelit'), 'name'=>'Penduduk Menurut Kabupaten Kota dan Jenis Kelamin, 2000-2013.xlsx', 'ext'=>'.xlsx'ender/Penduduk Menurut Kabupaten Kota dan Jenis Kelamin, 2000-2013.xlsx', 'slug'=>str_random(10), 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
        	]);
    }
}
