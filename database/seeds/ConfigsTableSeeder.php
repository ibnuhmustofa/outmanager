<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();

        DB::table('configs')->insert([
        		['ip_server'=>'192.168.1.254', 'upload_path'=>'D\Upload', 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()]
        	]);
    }
}
