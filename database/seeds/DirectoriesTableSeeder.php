<?php

use Illuminate\Database\Seeder;

class DirectoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directories')->delete();

        DB::table('directories')->insert([
                // 1
        		['parent_slug'=>'rtrw-provinsi', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-provinsi'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-provinsi', 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-provinsi', 'name'=>'2 Data dan Informasi', 'slug'=>substr(substr(base64_encode('rtrw-provinsi'), 0, 5).substr(base64_encode('data-dan-informasi'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-provinsi', 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-provinsi', 'name'=>'3 Kebijakan Sektoral', 'slug'=>substr(substr(base64_encode('rtrw-provinsi'), 0, 5).substr(base64_encode('kebijakan-sektoral'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-provinsi', 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                // 2
                ['parent_slug'=>'rtrw-kabupaten', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kabupaten'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-kabupaten', 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-kabupaten', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kabupaten'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-kabupaten', 'parent_no'=>2, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-kabupaten', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kabupaten'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>'rtrw-kabupaten', 'parent_no'=>3, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                // 3
                ['parent_slug'=>'rtrw-kota', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kota'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>3, 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-kota', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kota'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>3, 'parent_no'=>2, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'rtrw-kota', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('rtrw-kota'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>3, 'parent_no'=>3, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                // 4
                ['parent_slug'=>'kebijakan-sektoral', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('kebijakan-sektoral'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>4, 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'kebijakan-sektoral', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('kebijakan-sektoral'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>4, 'parent_no'=>2, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'kebijakan-sektoral', 'name'=>'1 Peta', 'slug'=>substr(substr(base64_encode('kebijakan-sektoral'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>4, 'parent_no'=>3, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                // 5
                ['parent_slug'=>'regulasi-tata-ruang', 'name'=>'Inpres', 'slug'=>substr(substr(base64_encode('regulasi-tata-ruang'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>5, 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'regulasi-tata-ruang', 'name'=>'Kepres', 'slug'=>substr(substr(base64_encode('regulasi-tata-ruang'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>5, 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
                ['parent_slug'=>'regulasi-tata-ruang', 'name'=>'Permen', 'slug'=>substr(substr(base64_encode('rtrw-provinsi'), 0, 5).substr(base64_encode('peta'), 0, 1).base64_encode(str_random(20)), 0, -2), 'top_parent'=>5, 'parent_no'=>1, 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],

        	]);
    }
}
