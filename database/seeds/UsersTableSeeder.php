<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
        		['name'=>'Demo User 1', 'email'=>'demo1@tjd.dev', 'password'=>bcrypt('demo'), 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
        		['name'=>'Demo User 2', 'email'=>'demo2@tjd.dev', 'password'=>bcrypt('demo'), 'created_at'=>new DateTime(), 'updated_at'=>new DateTime()],
        	]);
    }
}
