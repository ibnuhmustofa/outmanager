<div class="row">
	<div class="col-lg-1">
			<button class="btn btn-primary" data-toggle="modal" data-target="#newFolder">
				<i class="fa fa-fw fa-plus"></i>
				Folder
			</button>
		</div>
    <div class="col-lg-1">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#uploader">
        <i class="fa fa-fw fa-plus"></i>Upload
        </button>
    </div>
	<div class="col-lg-5 col-lg-offset-1">
			<form method="get" action="search">
		<div class="input-group top_search">
    	<input type="text" class="form-control" placeholder="Cari Berkas ..." name="query">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
          	<i class="fa fa-fw fa-search"></i>
          </button>
        </span>
  	</div>
  		</form>
	</div>
	<div class="col-lg-3 pull-right">
		<a href="{{ url('/tabulasi') }}" class="btn btn-info text-white">
			<i class="fa fa-fw fa-table"></i> Tabulasi
		</a>
		<div class="btn-group pull-right">
		<a href="{{ url('file') }}" class="btn btn-danger">Menu</a>
		<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="caret"></span>
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu">
		    <li><a href="{{ url('config') }}">Konfigurasi</a></li>
		    <li><a href="{{ url('config/app') }}">Aplikasi</a></li>
		    <li><a href="{{ url('log/updatefile') }}">Perbarui Aplikasi</a></li>
		    <li><a href="{{ url('log') }}">Log</a></li>
		    <li role="separator" class="divider"></li>
		    <li><a href="#">Logout</a></li>
		</ul>
	</div>
	</div>
</div>