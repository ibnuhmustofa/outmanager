<div class="container_inner">
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3 footer_text">
			<h5>Copyright &copy; @if(Carbon\Carbon::now()->year > 2016) 2016-{{ Carbon\Carbon::now()->year }} @else 2016 @endif - Dinas Cipta Karya dan Tata Ruang</h5>
		</div>
	</div>
</div>