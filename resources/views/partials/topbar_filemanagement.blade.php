<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Harus Diperbarui</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Harus Ada</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="overview">

      <div class="row">
        <div class="col-lg-12">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Semua</a></li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">
              <div class="row">
                <div class="col-lg-12">
                  <div class="well">
                    <div class="row">
                        <table class="table table-striped table-hovered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Berkas</th>
                              <th>Terakhir Diperbarui</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>Data Penduduk Tahun 2015</td>
                              <td>2 Maret 2015</td>
                              <td>
                                <button type="button" class="btn btn-success btn-xs" title="perbarui">
                                  <i class="fa fa-fw fa-edit"></i>
                                </button>
                              </td>
                            </tr>
                            <tr>
                              <td>2</td>
                              <td>Data Penduduk Tahun 2015</td>
                              <td>2 Maret 2015</td>
                              <td>
                                <button type="button" class="btn btn-success btn-xs" title="perbarui">
                                  <i class="fa fa-fw fa-edit"></i>
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>  
                  </div>
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>


        <!-- Harus ada -->
<!--         <div class="col-lg-6">
          <div class="well">
            <div class="row">
              <div class="col-lg-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Folder</th>
                      <th>Berkas</th>
                      <th>Ada</th>
                      <th>Tidak</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>1 Peta</td>
                      <td>Peta Kota Semarang RBI</td>
                      <td>
                        <i class="fa fa-fw fa-check"></i>
                      </td>
                      <td>
                        &nbsp;
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>1 Peta</td>
                      <td>Peta Kota Semarang RBI</td>
                      <td>
                        &nbsp;
                      </td>
                      <td>
                        <i class="fa fa-fw fa-check"></i>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>  
          </div>
        </div> -->
      </div>
    </div>
<!--     <div role="tabpanel" class="tab-pane" id="profile">...</div>
    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div> -->
  </div>

</div>