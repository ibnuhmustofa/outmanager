	<div class="col-xs-2"> <!-- required for floating -->
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs tabs-left">
	      <li @if(Request::path() == 'file') class="active" @endif><a href="{{ url('file') }}">Manajemen Berkas</a></li>
	      <li @if(Request::path() == 'config') class="active" @endif><a href="{{ url('config') }}">Konfigurasi</a></li>
	      <li @if(Request::path() == 'app') class="active" @endif><a href="{{ url('app') }}">Aplikasi</a></li>
	      <!-- <li @if(Request::path() == 'log/updatefile') class="active" @endif><a href="{{ url('log/updatefile') }}">Perbarui File</a></li> -->
	      <li @if(Request::path() == 'log') class="active" @endif><a href="{{ url('log') }}">Log</a></li>
	    </ul>
	</div>