<div class="top_heading">
   <div class="container_inner">
   		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
               <div class="col-lg-2 img_logo">
                  <img src="{{ url('images/jateng.png') }}">
               </div>
               <div class="col-lg-8 text-center main_header_text">
                  <h2>{{ config('local.heading1') }}</h2>
                  <h3>{{ config('local.heading2') }}</h3>
                  <h5>{{ config('local.heading3') }}</h5>
               </div>               
            </div>
   		</div>
   </div>
</div>