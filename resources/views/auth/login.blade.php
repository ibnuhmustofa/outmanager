<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('local.name') }} :: Login </title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}">
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/custom.css') !!}
</head>
<body class="login">
<main>
	<div>
	    <div class="login_wrapper">
	        <div class="animate form login_form">
	            <section class="login_content">
	                {!! Form::open() !!}
	                    <h1>LOG - IN</h1>
	                    <div>
	                        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Email']) !!}
	                    </div>
	                    <div>
	                        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password']) !!}
	                    </div>
	                    <div>
	                        <button type="submit" class="btn btn-primary btn-block">Login</button>
	                    </div>
	                {!! Form::close() !!}
	            </section>
	        </div>
	    </div>
	</div>
</main>
</body>
</html>