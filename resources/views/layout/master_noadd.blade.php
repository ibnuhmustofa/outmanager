<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>:: {{ config('local.name') }} :: @yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}">
    {!! Html::style('css/bootstrap.css') !!}
    {!! Html::style('css/font-awesome.min.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('css/bootstrap.vertical-tabs.css') !!}
    {!! Html::style('css/demo.css') !!}
    {!! Html::style('css/style4.css') !!}

    {!! Html::style('css/basic.css') !!}
    {!! Html::style('css/dropzone.css') !!}
    {!! Html::style('css/jquery.contextMenu.css') !!}
</head>
<body>
<!-- /***********************************************************/ -->

<?php
// Shell Command
// $command = "gpicview /home/helpdesk/Public/uploader/rtrw-provinsi/winbox.png";
// $output = shell_exec($command);
// "<pre>$output</pre>";
?>

<!-- ********************************************************************** -->


    <div class="container body">
      <div class="main_container">

       @include('partials.main_header')

       <div class="container_inner">
       	@include('partials.main_menu_noadd')
       </div>

      </div>

       <div class="container_inner">
            <div class="row">
                <div class="col-lg-12" style="border-bottom: 1px solid #999;">
                    <a href="{{ url('/') }}" style="font-size: 30px; color: #000;">
                        <i class="fa fa-fw fa-home"></i>
                    </a>
                </div>
            </div>
       </div>

	@yield('content')


<!-- Modal new folder -->
<div class="modal fade" id="newFolder" tabindex="-1" role="dialog" aria-labelledby="Folder Baru">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="true">&times;</span>
				</button>
				<h4 class="modal-title">Folder Baru</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						{!! Form::open(['action'=>'AppController@storeFolder']) !!}
							<div class="form-group">
								<input type="hidden" name="current" value="{{ Request::path() }}" />
								<input type="text" class="form-control" name="folderName" id="folderName" placeholder="Nama Folder" />
							</div>
							<div class="form-group">
								<div class="col-lg-8 col-lg-offset-4">
									<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
									<button type="submit" class="btn btn-primary">Buat</button>
								</div>
							</div>
						{!! Form::close() !!}	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Uploader -->
<div class="modal fade" id="uploader" tabindex="-1" role="dialog" aria-labelledby="Folder Baru">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="true">&times;</span>
				</button>
				<h4 class="modal-title">Upload File</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						{!! Form::open(['action'=>'AppController@storeFile', 'files'=>true, 'class'=>'dropzone', 'id'=>'uploader']) !!}
							<input type="hidden" name="current" value="{{ Request::path() }}" />
       						{!! Form::file('file', ['multiple', 'style'=>'overflow:hidden; opacity:0;']) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	{!! Html::script('js/jquery.min.js') !!}
	{!! Html::script('js/bootstrap.min.js') !!}
	{!! Html::script('js/custom.js') !!}
	{!! Html::script('js/dropzone.js') !!}
	{!! Html::script('js/jquery.contextMenu.js') !!}


	{!! Html::script('js/vendor/angularjs/angular.min.js') !!}
	{!! Html::script('js/vendor/angularjs/angular-messages.min.js') !!}

	{!! Html::script('js/app.js') !!}

	<script type="text/javascript">
	$(document).ready(function() {
		$('button.editEnable').click(function() {
			$(this).hide();
			$('span.editDisable').hide();
			$('span.editEnable').show();
			$('button.editDisable').show();
		});
	});
</script>

@yield('extender')

</body>
</html>