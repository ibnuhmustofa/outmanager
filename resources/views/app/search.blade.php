@extends('layout.master_noadd')

@section('title')
	Hasil Pencarian '{{ $query }}'
@endsection

@section('content')
<div class="container_inner">
	<h2>Hasil Pencarian</h2>
		<table class="table table-striped table-folder" style="margin-top: 20px;">
			<thead>
				<tr>
					<th>Nama</th>
					<th>&nbsp;</th>
					<th>Terakhir Diubah</th>
				</tr>
			</thead>
			<tbody>
			@foreach($src as $file)
				<tr class="a-file-{{ $file->id }}" access="{{ $file->alias_name }}" a-n="{{ $file->origin_name }}" a-d="{{ $file->id }}">
					<td>
						<a>
							<i class="fa fa-fw fa-file"></i> {{ str_limit($file->origin_name, 80) }}
						</a>
					</td>
					<td class="text-center f-date">
						{{ $file->updated_at->diffForHumans() }}
					</td>
				</tr>	
			@endforeach
			</tbody>
		</table>
</div>

<!-- File Modal -->
  <div class="modal fade" id="moFirena" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ganti Nama Berkas</h4>
        </div>
        <div class="modal-body">
          <div class="row">
          	<div class="col-lg-10 col-lg-offset-1">
  				{!! Form::open(['action'=>'AppController@updateFilename']) !!}
					<div class="form-group">
						<input type="hidden" name="fid" id="fid">
						<input type="text" class="form-control" name="fileName" id="fileName" placeholder="Nama File" />
					</div>
					<div class="form-group">
						<div class="col-lg-4 pull-right">
							<button type="submit" class="btn btn-primary btn-block">Ganti</button>
						</div>
						<div class="col-lg-4 pull-right">
							<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>
						</div>
					</div>
				{!! Form::close() !!}
          	</div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

    <!-- File Modal -->
  <div class="modal fade" id="moFide" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        	<div class="row">
        		<div class="col-lg-12">
        			<h4 class="modal-title">Hapus berkas "<span id="fideName"></span>" ?</h4><br>
        		</div>
        	</div>
    		{!! Form::open(['action'=>'AppController@deleteFile']) !!}
    			<input type="hidden" id="fide" name="fid">
				<div class="form-group">
					<div class="col-lg-4 col-lg-offset-2">
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
					</div>
					<div class="col-lg-4">
						<button type="submit" class="btn btn-danger btn-block">Ya</button>
					</div>
				</div>
			{!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>


<!-- Folder Modal -->
  <div class="modal fade" id="moForena" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ganti Nama Folder</h4>
        </div>
        <div class="modal-body">
          <div class="row">
          	<div class="col-lg-10 col-lg-offset-1">
  				{!! Form::open(['action'=>'AppController@updateFoldername']) !!}
					<div class="form-group">
						<input type="hidden" name="fod" id="fod">
						<input type="text" class="form-control" name="folderName" id="folderName" placeholder="Nama Folder" />
					</div>
					<div class="form-group">
						<div class="col-lg-4 pull-right">
							<button type="submit" class="btn btn-primary btn-block">Ganti</button>
						</div>
						<div class="col-lg-4 pull-right">
							<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>
						</div>
					</div>
				{!! Form::close() !!}
          	</div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Folder Modal -->
  <div class="modal fade" id="moFode" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        	<div class="row">
        		<div class="col-lg-12">
        			<h4 class="modal-title">Hapus folder "<span id="fodeName"></span>" ?</h4><br>
        		</div>
        	</div>
    		{!! Form::open(['action'=>'AppController@deleteFolder']) !!}
    			<input type="hidden" id="fode" name="fod">
				<div class="form-group">
					<div class="col-lg-4 col-lg-offset-2">
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
					</div>
					<div class="col-lg-4">
						<button type="submit" class="btn btn-danger btn-block">Ya</button>
					</div>
				</div>
			{!! Form::close() !!}
        </div>
      </div>
      
    </div>
  </div>
@endsection

@section('extender')
<script type="text/javascript">
  	$(function() {
  		@foreach($src as $file)
			$.contextMenu({
				selector: '.a-file-{{ $file->id }}',
				callback: function(key, options) {
					var m = 'clicked: '+key;
					window.console && console.log(m) || alert(m); 
				},
				items: {
					// "open": {
					// 	name: "Buka",
					// 	icon: "fa-external-link",
					// 	callback: function() {
					// 		var access = $('.a-file-{{ $file->id }}').attr('access');
					// 		window.location.href = 'ap/fi/'+access;
					// 	}
					// },
					"rename": {
						name: "Ganti Nama",
						icon: "fa-edit",
						callback: function($id) {
							$('#moFirena').modal('show');
							var fname = $('.a-file-{{ $file->id }}').attr('a-n');
							$('#fileName').val(fname);
							$('#fid').val($('.a-file-{{ $file->id }}').attr('a-d'));
						}
					},
					"delete": {
						name: "Hapus Berkas",
						icon: "fa-trash",
						callback: function() {
							$('#moFide').modal('show');
							$('#fideName').append($('.a-file-{{ $file->id }}').attr('a-n'));
							$('#fide').val($('.a-file-{{ $file->id }}').attr('a-d'));
						}
					},
				}
			});
		@endforeach
  	});
  </script>
@endsection