@extends('layout.master_app')

@section('content')
<div class="container_inner">
		<table class="table table-striped table-folder" style="margin-top: 20px;">
			<thead>
				<tr>
					<th>Nama</th>
					<th>&nbsp;</th>
					<th>Terakhir Diubah</th>
				</tr>
			</thead>
			<tbody>
			@foreach($subdirs as $sub)
				<tr>
					<td>
						<a href="{{ $sub->slug }}">
							<i class="fa fa-fw fa-folder"></i> <?php echo str_limit($sub->name, 80); ?>
						</a>
					</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle btn-option" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-fw fa-ellipsis-h"></i>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Ganti Nama</a>
									<a href="#">Hapus</a>
								</li>
							</ul>
						</div>
					</td>
					<td>
						{{ $sub->updated_at->diffForHumans() }}
					</td>
				</tr>
			@endforeach
			@foreach($files as $file)
				<tr>
					<td>
						<a href="file://{{ $file->file }}">
							<i class="fa fa-fw fa-file" style="color: #999"></i> <?php echo str_limit($file->name, 80); ?>
						</a>
					</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle btn-option" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-fw fa-ellipsis-h"></i>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="file://{{ $file->file }}" role="link">Buka</a>
									<a href="#">Buka di Explorer</a>
									<a href="#">Ganti Nama</a>
									<a href="#">Hapus</a>
								</li>
							</ul>
						</div>
					</td>
					<td>
						{{ $file->updated_at->diffForHumans() }}
					</td>
				</tr>			
			@endforeach
			</tbody>
		</table>
</div>
@endsection