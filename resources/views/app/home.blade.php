<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>:: {{ config('local.name') }} :: @yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}">
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/font-awesome.min.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('css/demo.css') !!}
    {!! Html::style('css/style4.css') !!}
</head>
<body>
<!-- /***********************************************************/ -->

<?php
// Shell Command
// $command = "pcmanfm /home/helpdesk/Public/uploader";
// $output = shell_exec($command);
// "<pre>$output</pre>";
?>

<!-- ********************************************************************** -->


    <div class="container body">
      <div class="main_container">
	      @include('partials.main_header')

	      <div class="container_inner">

		      @include('partials.main_menu_noadd')	
	      </div>
       <div class="container_inner">
            <div class="row">
                <div class="col-lg-12" style="border-bottom: 1px solid #999;">
                    <a href="{{ url('/') }}" style="font-size: 30px; color: #000;">
                        <i class="fa fa-fw fa-home"></i>
                    </a>
                </div>
            </div>
       </div>

    <div class="content">	
	    <div class="content">
			<ul class="ca-menu">
	            <li>
	                <a href="{{ url('/rtrw-provinsi') }}">
	                    <span class="ca-icon">
	                    	<i class="fa fa-fw fa-globe"></i>
	                    </span>
	                    <div class="ca-content">
	                        <h2 class="ca-main">RTRW</h2>
	                        <h2 class="ca-main">Provinsi Jateng</h2>
	                        <h3 class="ca-sub">Folder</h3>
	                    </div>
	                </a>
	            </li>
	            <li>
	                <a href="{{ url('/rtrw-kabupaten') }}">
	                    <span class="ca-icon">
	                    	<i class="fa fa-fw fa-globe"></i>
	                    </span>
	                    <div class="ca-content">
	                        <h2 class="ca-main">RTRW</h2>
	                        <h2 class="ca-main">Kabupaten</h2>
	                        <h3 class="ca-sub">Folder</h3>
	                    </div>
	                </a>
	            </li>
	            <li>
	                <a href="{{ url('/rtrw-kota') }}">
	                    <span class="ca-icon">
	                    	<i class="fa fa-fw fa-globe"></i>
	                    </span>
	                    <div class="ca-content">
	                        <h2 class="ca-main">RTRW</h2>
	                        <h2 class="ca-main">Kota</h2>
	                        <h3 class="ca-sub">Folder</h3>
	                    </div>
	                </a>
	            </li>
	            <li>
	                <a href="{{ url('/kebijakan-sektoral') }}">
	                    <span class="ca-icon">
	                    	<i class="fa fa-fw fa-globe"></i>
	                    </span>
	                    <div class="ca-content">
	                        <h2 class="ca-main">Kebijakan Sektoral</h2>
	                        <h3 class="ca-sub">Folder</h3>
	                    </div>
	                </a>
	            </li>
	            <li>
	                <a href="{{ url('/regulasi-tata-ruang') }}">
	                    <span class="ca-icon">
	                    	<i class="fa fa-fw fa-globe"></i>
	                    </span>
	                    <div class="ca-content">
	                        <h2 class="ca-main">Regulasi Tata Ruang</h2>
	                        <h3 class="ca-sub">Folder</h3>
	                    </div>
	                </a>
	            </li>
	        </ul>
		</div>
	</div>   


<!-- Modal new folder -->
<div class="modal fade" id="newFolder" tabindex="-1" role="dialog" aria-labelledby="Folder Baru">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="true">&times;</span>
				</button>
				<h4 class="modal-title">Folder Baru</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						{!! Form::open(['action'=>'AppController@storeFolder']) !!}
							<div class="form-group">
								<input type="hidden" name="current" value="{{ Request::path() }}" />
								<input type="text" class="form-control" name="folderName" id="folderName" placeholder="Nama Folder" />
							</div>
							<div class="form-group">
								<div class="col-lg-8 col-lg-offset-4">
									<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
									<button type="submit" class="btn btn-primary">Buat</button>
								</div>
							</div>
						{!! Form::close() !!}	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	{!! Html::script('js/jquery.min.js') !!}
	{!! Html::script('js/bootstrap.min.js') !!}
	{!! Html::script('js/custom.js') !!}

	@include('partials.main_footer')
</body>
</html>