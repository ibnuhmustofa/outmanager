@extends('layout.master_noadd')

@section('title')
	Tabulasi
@endsection

@section('crumbcontent')
	<li>
		<a href="#">Tabulasi</a>
	</li>
@endsection

@section('content')
<div class="container_inner container_bg">
	<div class="row">
		<div class="col-lg-12">
			<h2>Rekapitulasi Berkas</h2>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['action'=>'AppController@storeMustexistfile', 'class'=>'form-horizontal']) !!}
				<!-- <div class="row">
					<div class="col-lg-1">
						Data Berkas
					</div>
					<div class="col-lg-2">
						<select class="form-control" name="dir">
				    		<option selected="true" disabled="true">- Pilih Direktori -</option>
				    		<option value="rtrw-provinsi">RTRW Provinsi</option>
				    		<option value="rtrw-kabupaten">RTRW Kabupaten</option>
				    		<option value="rtrw-kota">RTRW Kota</option>
				    		<option value="kebijakan-sektoral">Kebijakan Sektoral</option>
				    		<option value="regulasi-tata-ruang">Regulasi Tata Ruang</option>
				    	</select>
					</div>
					<div class="col-lg-2">
						<input type="text" class="form-control" id="fileName" placeholder="Nama Berkas">
					</div>
					<div class="col-lg-1">
					    <input type="text" class="form-control" id="fileExt" placeholder="Eks">
					</div>
				</div>
				  <button type="submit" class="btn btn-primary" style="position:relative; top:2px;">Tambah</button> -->

				  <div class="form-group">
				    <label for="dir" class="col-sm-2 control-label">Direktori</label>
				    <div class="col-sm-3">
				    	<select class="form-control" name="dir" required="true">
				    		<option selected="true" disabled="true">- Pilih Direktori -</option>
				    		@foreach($dirs as $dir)
				    		<option value="{{ $dir->slug }}">{{ $dir->name }}</option>
				    		@endforeach
				    	</select>
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="fileName" class="col-sm-2 control-label">File</label>
				    <div class="col-sm-4">
				      <input type="name" class="form-control" id="fileName" name="fileName" placeholder="Nama File" required="true">
				    </div>
				    <div class="col-sm-1" >
				    	<input type="text" name="fileExt" placeholder="Ekstensi" class="form-control" required="true">
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-3">
				      <button type="submit" class="btn btn-primary block">Simpan</button>
				    </div>
				  </div>
			{!! Form::close() !!}
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th rowspan="2">No</th>
						<th rowspan="2">Main Folder</th>
						<th rowspan="2">Sub Folder</th>
						<th rowspan="2">Berkas</th>
						<th rowspan="2">Ekstensi</th>
						<th colspan="2">Ketersediaan</th>
					</tr>
					<tr>
						<th>Ada</th>
						<th>Tidak</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($tabus as $tabu)
					<tr @if($tabu->uploaded == 0) class="danger" @endif>
						<td class="text-center">{{ $i++ }}</td>
						<td>
							<?php
								switch ($tabu->main_directory) {
									case 'rtrw-provinsi':
										echo "RTRW Provinsi Jateng";
										break;
									case 'rtrw-kabupaten':
										echo "RTRW Kabupaten";
										break;
									
									default:
										echo "-";
										break;
								}
							?>
						</td>
						<td>{{ $tabu->sub_directory }}</td>
						<td>{{ $tabu->file_name }}</td>
						<td class="text-center">{{ $tabu->ext }}</td>
						<td class="text-center text-success">
							@if($tabu->uploaded == 1) <i class="fa fa-fw fa-check"></i> @else &nbsp; @endif
						</td>
						<td class="text-center text-danger">
							@if($tabu->uploaded == 0) <i class="fa fa-fw fa-check"></i> @else &nbsp; @endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
