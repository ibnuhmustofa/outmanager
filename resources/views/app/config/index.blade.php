@extends('layout.master_noadd')

@section('title')
	Konfigurasi
@endsection

@section('content')
<div class="container_inner">
	<br>
	@include('partials.sidebar_config')

	<div class="col-xs-9">
	    <!-- Tab panes -->
	    <div class="tab-content">
		    <div id="home">
		    	<div class="row">
		    		<div class="col-lg-12 page-heading">
		    			<h3>Konfigurasi</h3>
		    			<hr>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-lg-12">
				      	{!! Form::open(['action'=>'ConfigController@updateUploadpath', 'class'=>'form-horizontal']) !!}
				      		<div class="form-group">
							    <label for="path" class="col-sm-2 control-label">IP Server</label>
							    <div class="col-sm-3">
							    	<input type="text" class="form-control" name="ip" id="ip" placeholder="Contoh : 192.168.1.1">
							    </div>
							    <div class="col-sm-3">
							    	<span class="help-block">Saat ini : 192.168.3.254</span>
							    </div>
							</div>
			    			<div class="form-group">
							    <label for="path" class="col-sm-2 control-label">Direktori Upload</label>
							    <div class="col-sm-3">
							    	<input type="text" class="form-control" name="path" id="path" placeholder="Contoh : D:\Upload">
							    </div>
							    <div class="col-sm-3">
							    	<span class="help-block">Saat ini : D:\Server\Upload</span>
							    </div>
							</div>
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									{!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-block']) !!}
								</div>
<!-- 								<div class="col-sm-3">
									<span id="helpBlock" class="help-block">Saat ini : @foreach($config as $conf) {{ $conf->upload_path }} @endforeach</span>	
								</div> -->
							</div>
				      	{!! Form::close() !!}
			      	</div>
		    	</div>

		    </div>
	    </div>
	</div>
</div>
@endsection