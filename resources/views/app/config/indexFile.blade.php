@extends('layout.master_noadd')

@section('title')
	Aplikasi Pembuka
@endsection

@section('content')
<div class="container_inner">
	<br>
	@include('partials.sidebar_config')
	<div class="col-xs-10">
		<div class="tab-content">
	    	<div class="row">
	    		<div class="col-lg-12 page-heading">
	    			<h3>Manajemen Berkas</h3>
	    			<hr>
	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-lg-12">
	    			@include('partials.topbar_filemanagement')
	    		</div>
	    	</div>
	    </div>
	</div>
</div>
@endsection