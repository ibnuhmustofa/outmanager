@extends('layout.master_noadd')

@section('title')
	Aplikasi Pembuka
@endsection

@section('content')
<div class="container_inner">
	<br>
	@include('partials.sidebar_config')

	<div class="col-xs-9">
		<div class="tab-content">
		    <div id="apps">
		    	<div class="row">
		    		<div class="col-lg-12 page-heading">
		    			<h3>Aplikasi Pembuka</h3>
		    			<hr>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-lg-12">
		    			<table class="table table-striped">
		    				<thead>
		    					<tr>
		    						<th>No</th>
		    						<th>Ekstensi</th>
		    						<th>Command Aplikasi</th>
		    						<th>&nbsp;</th>
		    					</tr>
		    				</thead>
		    				<tbody>
		    					<?php $no = 1; ?>
	    						@foreach($opener as $file)
		    					<tr>
		    						<td style="text-align:center">{{ $no++ }}</td>
		    						<td>{{ $file->ext }}</td>
		    						<td style="text-align:center">
		    							<span ng-hide="editEnable">
		    								{{ $file->app_command }}</td>
		    							</span>
<!-- 		    							<span ng-show="editEnable">
		    								<input type="text" class="form-control" name="apps" value="{{ $file->app_command }}">
		    							</span> -->
		    						<td style="text-align:center">
		    							<span ng-show="editEnable">
			    							<button type="button" class="btn btn-success btn-xs" ng-click="editEnable!=editEnable">
			    								<i class="fa fa-fw fa-edit"></i>
			    							</button>
		    							</span>
<!-- 		    							<span ng-hide="editEnable">
		    								<button type="button" class="btn btn-info btn-xs" ng-click="editEnable!=editEnable">
			    								<i class="fa fa-fw fa-check"></i>
			    							</button>
		    							</span> -->
		    						</td>
		    					</tr>
		    					@endforeach
		    				</tbody>
		    			</table>
		    		</div>
		    	</div>
		    </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	
</script>
@endsection